import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { ChartsModule } from "ng2-charts";

import {
  MatSidenavModule, MatDividerModule, MatListModule, MatExpansionModule, MatDatepickerModule, MatSelectModule,
  MatIconModule, MatCardModule, MatInputModule, MatButtonModule, MatTableModule, MatPaginatorModule, MatSortModule,
  MatProgressBarModule
} from '@angular/material';
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';


import { AsideComponent } from './theme/aside/aside.component';
import { BaseComponent } from './theme/base/base.component';
import { HeaderComponent } from './theme/header/header.component';
import { FooterComponent } from './theme/footer/footer.component';
import { ErrorComponent } from './theme/error/error.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { MoreDetailsTableComponent } from './pages/dashboard/more-details-table/more-details-table.component';
import { CutOffDateAndTimeComponent } from './pages/report/cut-off-date-and-time/cut-off-date-and-time.component';
import { SummeryReportComponent } from './pages/report/summery-report/summery-report.component';
import { PrintingComponent } from './pages/printing/printing.component';
import { ImsReportsComponent } from './pages/ims-reports/ims-reports.component';
import { OnYourMarkComponent } from './pages/on-your-mark/on-your-mark.component';
import { FileUploadComponent } from './pages/file-upload/file-upload.component';
import { MedicalDecisionComponent } from './pages/dashboard/more-details-table/medical-decision/medical-decision.component';
import { MoreDetailsAutoTableComponent } from './pages/dashboard/more-details-auto-table/more-details-auto-table.component';
import { MoreDetailsManualTableComponent } from './pages/dashboard/more-details-manual-table/more-details-manual-table.component';
import { MedicalAutoDecisionComponent } from './pages/dashboard/more-details-auto-table/medical-auto-decision/medical-auto-decision.component';
import { MedicalManualDecisionComponent } from './pages/dashboard/more-details-manual-table/medical-manual-decision/medical-manual-decision.component';
import { DashboardSearchComponent } from './pages/dashboard/dashboard-search/dashboard-search.component';
import { GetBranchHistoryComponent } from './pages/dashboard/dashboard-search/get-branch-history/get-branch-history.component';
import { ActivityComponent } from './pages/kpi/activity/activity.component';
import { StpComponent } from './pages/kpi/stp/stp.component';
import { AdoptionComponent } from './pages/kpi/adoption/adoption.component';
import { SubmissionComponent } from './pages/kpi/submission/submission.component';
import { PolicyComponent } from './pages/kpi/policy/policy.component';
import { UserBasedComponent } from './pages/kpi/user-based/user-based.component';
import { HistoryComponent } from './pages/history/history.component';
import { MedicalHistoryComponent } from './pages/history/medical-history/medical-history.component';
import { AutoMedicalHistoryComponent } from './pages/history/auto-medical-history/auto-medical-history.component';
import { ManualMedicalHistoryComponent } from './pages/history/manual-medical-history/manual-medical-history.component';

@NgModule({
  declarations: [
    AppComponent,
    AsideComponent,
    BaseComponent,
    HeaderComponent,
    FooterComponent,
    ErrorComponent,
    LoginComponent,
    DashboardComponent,
    MoreDetailsTableComponent,
    CutOffDateAndTimeComponent,
    SummeryReportComponent,
    PrintingComponent,
    ImsReportsComponent,
    OnYourMarkComponent,
    FileUploadComponent,
    MedicalDecisionComponent,
    MoreDetailsAutoTableComponent,
    MoreDetailsManualTableComponent,
    MedicalAutoDecisionComponent,
    MedicalManualDecisionComponent,
    DashboardSearchComponent,
    GetBranchHistoryComponent,
    ActivityComponent,
    StpComponent,
    AdoptionComponent,
    SubmissionComponent,
    PolicyComponent,
    UserBasedComponent,
    HistoryComponent,
    MedicalHistoryComponent,
    AutoMedicalHistoryComponent,
    ManualMedicalHistoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatDividerModule,
    MatListModule,
    MatExpansionModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatSelectModule,
    MatProgressBarModule,
    SatDatepickerModule, 
    SatNativeDateModule,
    ReactiveFormsModule,
    FormsModule,
    ChartsModule,
    MatMenuModule,
    MatToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
