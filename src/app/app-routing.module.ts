import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseComponent } from './theme/base/base.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
import { MoreDetailsTableComponent } from './pages/dashboard/more-details-table/more-details-table.component';
import { SummeryReportComponent } from './pages/report/summery-report/summery-report.component';
import { CutOffDateAndTimeComponent } from './pages/report/cut-off-date-and-time/cut-off-date-and-time.component';
import { PrintingComponent } from './pages/printing/printing.component';
import { ImsReportsComponent } from './pages/ims-reports/ims-reports.component';
import { OnYourMarkComponent } from './pages/on-your-mark/on-your-mark.component';
import {FileUploadComponent} from './pages/file-upload/file-upload.component';
import {MedicalDecisionComponent} from './pages/dashboard/more-details-table/medical-decision/medical-decision.component';
import { MoreDetailsAutoTableComponent } from './pages/dashboard/more-details-auto-table/more-details-auto-table.component';
import { MoreDetailsManualTableComponent } from './pages/dashboard/more-details-manual-table/more-details-manual-table.component';
import { MedicalAutoDecisionComponent } from './pages/dashboard/more-details-auto-table/medical-auto-decision/medical-auto-decision.component';
import { MedicalManualDecisionComponent } from './pages/dashboard/more-details-manual-table/medical-manual-decision/medical-manual-decision.component';
import { DashboardSearchComponent } from './pages/dashboard/dashboard-search/dashboard-search.component';
import { GetBranchHistoryComponent } from './pages/dashboard/dashboard-search/get-branch-history/get-branch-history.component';
import { ActivityComponent } from './pages/kpi/activity/activity.component';
import { StpComponent } from './pages/kpi/stp/stp.component';
import { AdoptionComponent } from './pages/kpi/adoption/adoption.component';
import { SubmissionComponent } from './pages/kpi/submission/submission.component';
import { PolicyComponent } from './pages/kpi/policy/policy.component';
import { UserBasedComponent } from './pages/kpi/user-based/user-based.component';
import { HistoryComponent } from './pages/history/history.component';
import { MedicalHistoryComponent } from './pages/history/medical-history/medical-history.component';
import { AutoMedicalHistoryComponent } from './pages/history/auto-medical-history/auto-medical-history.component';
import { ManualMedicalHistoryComponent } from './pages/history/manual-medical-history/manual-medical-history.component';

const routes: Routes = [
  {
    path: '',
    component: BaseComponent,
    children: [
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'auto-medical-history',
        component: AutoMedicalHistoryComponent
      },
      {
        path: 'manual-medical-history',
        component: ManualMedicalHistoryComponent
      },
      {
        path: 'dashboard-more-details',
        component: MoreDetailsTableComponent
      },
      {
        path: 'medical-history',
        component: MedicalHistoryComponent
      },
      {
        path: 'history',
        component: HistoryComponent
      },
      {
        path: 'dashboard-more-auto-details',
        component: MoreDetailsAutoTableComponent
      },
      {
        path: 'dashboard-more-manual-details',
        component: MoreDetailsManualTableComponent
      },
      {
        path: 'summery-report',
        component: SummeryReportComponent
      },
      {
        path: 'cut-off-date-and-time',
        component: CutOffDateAndTimeComponent
      },
      {
        path: 'printing',
        component: PrintingComponent
      },
      {
        path: 'reports-ims-reports',
        component: ImsReportsComponent
      },
      {
        path: 'reports-on-your-mark',
        component: OnYourMarkComponent
      },
      {
        path: 'fileupload',
        component: FileUploadComponent
      },
      {
        path: 'medical-decision',
        component: MedicalDecisionComponent
      },
      {
        path: 'medical-auto-decision',
        component: MedicalAutoDecisionComponent
      },
      {
        path: 'medical-manual-decision',
        component: MedicalManualDecisionComponent
      },
      {
        path: 'dashboard-search',
        component: DashboardSearchComponent
      },
      {
        path: 'get-branch-history',
        component: GetBranchHistoryComponent
      },
      {
        path: 'activity',
        component: ActivityComponent
      }, 
      {
        path: 'stp',
        component: StpComponent
      },
      {
        path: 'adoption',
        component: AdoptionComponent
      },
      {
        path: 'submission',
        component: SubmissionComponent
      },
      {
        path: 'policy',
        component: PolicyComponent
      },
      {
        path: 'user-based',
        component: UserBasedComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
