import { Component, OnInit } from '@angular/core';

import { Branch, MainCategory } from "./../../../core/interface/common.model";
import { Status } from "./../../../core/interface/reports.model";
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-summery-report',
  templateUrl: './summery-report.component.html',
  styleUrls: ['./summery-report.component.css']
})
export class SummeryReportComponent implements OnInit {

  form: FormGroup;

  branches: Branch[] = [
    {
      value: '0',
      branch: 'Galle'
    },
    {
      value: '1',
      branch: 'Colombo'
    }
  ];

  mainCategories: MainCategory[] = [
    {
      value: '0',
      category: 'All'
    },
    {
      value: '1',
      category: 'Medical'
    },
    {
      value: '2',
      category: 'Non Medical-Auto'
    },
    {
      value: '3',
      category: 'Non Medical-Manual'
    },
  ];

  statuses: Status[] = [
    {
      value: '0',
      status: 'Accepted'
    },
    {
      value: '1',
      status: 'Pending'
    },
    {
      value: '2',
      status: 'Decline'
    },
    {
      value: '3',
      status: 'Postpone(3 month)'
    },
    {
      value: '4',
      status: 'Postpone(6 month)'
    },
    {
      value: '5',
      status: 'Postpone(12 month)'
    },
    {
      value: '6',
      status: 'Cancel'
    },
    {
      value: '7',
      status: 'Other'
    }
  ];

  constructor(fb: FormBuilder) { 
    this.form = fb.group({
      date1: [{begin: new Date(), end: new Date()}],
      date2: [{begin: new Date(), end: new Date()}]
    });
  }

  ngOnInit() {
    
  }

}
