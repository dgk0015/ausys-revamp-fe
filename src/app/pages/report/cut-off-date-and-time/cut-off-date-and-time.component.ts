import { Component, OnInit, ViewChild } from '@angular/core';

import { Eligibility, Year, Month, MainCategory } from "./../../../core/interface/common.model";
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { CutOffDateTimeTableData } from "./../../../core/interface/reports.model";
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-cut-off-date-and-time',
  templateUrl: './cut-off-date-and-time.component.html',
  styleUrls: ['./cut-off-date-and-time.component.css']
})
export class CutOffDateAndTimeComponent implements OnInit {

  displayedColumns: string[] = ['proposalNo', 'branch', 'agent', 'mainUrType', 'status'];
  dataSource = new MatTableDataSource<CutOffDateTimeTableData>(REPORTS_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  years: Year[] = [
    {
      value: '0',
      year: '2019'
    },
    {
      value: '1',
      year: '2020'
    }
  ];

  months: Month[] = [
    {
      value: '0',
      month: 'January'
    },
    {
      value: '1',
      month: 'February'
    },
    {
      value: '2',
      month: 'March'
    },
    {
      value: '3',
      month: 'April'
    },
    {
      value: '4',
      month: 'May'
    },
    {
      value: '5',
      month: 'June'
    },
    {
      value: '6',
      month: 'July'
    },
    {
      value: '7',
      month: 'August'
    },
    {
      value: '8',
      month: 'September'
    },
    {
      value: '9',
      month: 'October'
    },
    {
      value: '10',
      month: 'November'
    },
    {
      value: '11',
      month: 'December'
    },
  ];

  mainCategories: MainCategory[] = [
    {
      value: '0',
      category: 'All'
    },
    {
      value: '1',
      category: 'Medical'
    },
    {
      value: '2',
      category: 'Non Medical-Auto'
    },
    {
      value: '3',
      category: 'Non Medical-Manual'
    },
  ];

  eligibilities: Eligibility[] = [
    {
      value: '0',
      eligibility: 'Eligible'
    },
    {
      value: '1',
      eligibility: 'Not-Eligible'
    }
  ];



  constructor() { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}

const REPORTS_DATA: CutOffDateTimeTableData[] = [
  {
    proposalNo: '123',
    branch: 'Galle',
    agent: 'Amal Kamal',
    mainUrType: 'Type1',
    status: 'Status1'
  },
  {
    proposalNo: '124',
    branch: 'Colombo',
    agent: 'Kamal Kamal',
    mainUrType: 'Type2',
    status: 'Status3'
  },
  {
    proposalNo: '121',
    branch: 'Gampaha',
    agent: 'Amal Kamal',
    mainUrType: 'Type5',
    status: 'Status2'
  },
  {
    proposalNo: '125',
    branch: 'Kandy',
    agent: 'Amal Kamal',
    mainUrType: 'Type4',
    status: 'Status4'
  },
  {
    proposalNo: '123',
    branch: 'Galle',
    agent: 'Amal Kamal',
    mainUrType: 'Type1',
    status: 'Status1'
  },
  {
    proposalNo: '123',
    branch: 'Galle',
    agent: 'Amal Kamal',
    mainUrType: 'Type1',
    status: 'Status1'
  },
  {
    proposalNo: '123',
    branch: 'Galle',
    agent: 'Amal Kamal',
    mainUrType: 'Type1',
    status: 'Status1'
  },
  {
    proposalNo: '123',
    branch: 'Galle',
    agent: 'Amal Kamal',
    mainUrType: 'Type1',
    status: 'Status1'
  },
  {
    proposalNo: '123',
    branch: 'Galle',
    agent: 'Amal Kamal',
    mainUrType: 'Type1',
    status: 'Status1'
  },
  {
    proposalNo: '123',
    branch: 'Galle',
    agent: 'Amal Kamal',
    mainUrType: 'Type1',
    status: 'Status1'
  }
]