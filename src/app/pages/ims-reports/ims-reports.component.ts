import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  proposalno: string;
  policyno: string;
  proposaldate: string;
  level1: string;
  level2: string;
  start: string;
  end: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {proposalno: '', policyno: '', proposaldate: '', level1: '', level2: '', start: '', end: ''},
  {proposalno: '', policyno: '', proposaldate: '', level1: '', level2: '', start: '', end: ''},
  {proposalno: '', policyno: '', proposaldate: '', level1: '', level2: '', start: '', end: ''},
];

@Component({
  selector: 'app-ims-reports',
  templateUrl: './ims-reports.component.html',
  styleUrls: ['./ims-reports.component.css']
})
export class ImsReportsComponent implements OnInit {

  displayedColumns: string[] = ['proposalno', 'policyno', 'proposaldate', 'level1', 'level2', 'start', 'end'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
