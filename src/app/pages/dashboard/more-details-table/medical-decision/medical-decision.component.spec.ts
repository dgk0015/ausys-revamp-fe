import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalDecisionComponent } from './medical-decision.component';

describe('MedicalDecisionComponent', () => {
  let component: MedicalDecisionComponent;
  let fixture: ComponentFixture<MedicalDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
