import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalAutoDecisionComponent } from './medical-auto-decision.component';

describe('MedicalAutoDecisionComponent', () => {
  let component: MedicalAutoDecisionComponent;
  let fixture: ComponentFixture<MedicalAutoDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalAutoDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalAutoDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
