import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoreDetailsAutoTableComponent } from './more-details-auto-table.component';

describe('MoreDetailsAutoTableComponent', () => {
  let component: MoreDetailsAutoTableComponent;
  let fixture: ComponentFixture<MoreDetailsAutoTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoreDetailsAutoTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoreDetailsAutoTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
