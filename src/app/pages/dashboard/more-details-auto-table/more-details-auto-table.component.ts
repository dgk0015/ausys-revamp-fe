import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  proposalno: string;
  proposalholder: string;
  agent: string;
  branch: string;
  issueddate: string;
  eligibility: string;
  validity: string;
  action: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {proposalno: '', proposalholder:'', agent: '', branch: '', issueddate: '', eligibility: '', validity: '', action: ''},
  {proposalno: '', proposalholder:'', agent: '', branch: '', issueddate: '', eligibility: '', validity: '', action: ''},
  {proposalno: '', proposalholder:'', agent: '', branch: '', issueddate: '', eligibility: '', validity: '', action: ''},
];

@Component({
  selector: 'app-more-details-auto-table',
  templateUrl: './more-details-auto-table.component.html',
  styleUrls: ['./more-details-auto-table.component.css']
})
export class MoreDetailsAutoTableComponent implements OnInit {

  displayedColumns: string[] = ['proposalno', 'proposalholder', 'agent', 'branch', 'issueddate', 'eligibility', 'validity', 'action'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
