import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalManualDecisionComponent } from './medical-manual-decision.component';

describe('MedicalManualDecisionComponent', () => {
  let component: MedicalManualDecisionComponent;
  let fixture: ComponentFixture<MedicalManualDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalManualDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalManualDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
