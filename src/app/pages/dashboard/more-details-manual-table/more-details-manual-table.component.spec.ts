import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoreDetailsManualTableComponent } from './more-details-manual-table.component';

describe('MoreDetailsManualTableComponent', () => {
  let component: MoreDetailsManualTableComponent;
  let fixture: ComponentFixture<MoreDetailsManualTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoreDetailsManualTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoreDetailsManualTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
