import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
status: string;
decision: string;
name: string;
date: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
{status: '', decision: '', name: '',  date: ''},
];

@Component({
  selector: 'app-get-branch-history',
  templateUrl: './get-branch-history.component.html',
  styleUrls: ['./get-branch-history.component.css']
})
export class GetBranchHistoryComponent implements OnInit {

  displayedColumns: string[] = ['status', 'decision', 'name', 'date'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
