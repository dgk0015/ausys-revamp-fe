import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetBranchHistoryComponent } from './get-branch-history.component';

describe('GetBranchHistoryComponent', () => {
  let component: GetBranchHistoryComponent;
  let fixture: ComponentFixture<GetBranchHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetBranchHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetBranchHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
