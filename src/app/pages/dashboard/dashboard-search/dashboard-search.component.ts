import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  proposalno: string;
  policyno: string;
  category: string;
  proposalholder: string;
  branch: string;
  status: string;
  agent: string;
  update: string;
  time: string;
  submission: string;
  validity: string;
  call: string;
  history: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {proposalno: '', policyno: '', category: '',  proposalholder: '', branch: '', status: '', agent: '', update: '', time: '', submission: '', validity: '', call: '', history: ''},
];


@Component({
  selector: 'app-dashboard-search',
  templateUrl: './dashboard-search.component.html',
  styleUrls: ['./dashboard-search.component.css']
})
export class DashboardSearchComponent implements OnInit {

  displayedColumns: string[] = ['proposalno', 'policyno', 'category', 'proposalholder', 'branch', 'status', 'agent', 'update', 'time', 'submission', 'validity', 'call', 'history'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
