import { Component, OnInit } from '@angular/core';

interface Food {
  value: string;
  viewValue: string;
}

export interface PeriodicElement {
  name: string;
  type: string;
  size: string;
  progress: string;
  status: string;
  actions: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {name: '', type: '', size: '', progress: '', status: '', actions: ''},
  {name: '', type: '', size: '', progress: '', status: '', actions: ''},
  {name: '', type: '', size: '', progress: '', status: '', actions: ''},
];

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {


  options = [{ value: "This is value 1", checked: true }];
  statuses = ["control"];

  // name = "Angular";//
  fileToUpload: any;
  imageUrl: any;
  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);

    //Show image preview
    let reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    };
    reader.readAsDataURL(this.fileToUpload);
  }


  displayedColumns: string[] = ['name', 'type', 'size', 'progress', 'status', 'actions'];
  dataSource = ELEMENT_DATA;


 foods: Food[] = [
  {value: 'age-0', viewValue: 'Age'},
  {value: 'medical-1', viewValue: 'Medical'},
  {value: 'bso-2', viewValue: 'BSO'},
  {value: 'other-3', viewValue: 'Other'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
