import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  proposalno: string;
  proposalholder: string;
  agent: string;
  branch: string;
  issueddate: string;
  action: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {proposalno: '', proposalholder:'', agent: '', branch: '', issueddate: '', action: ''},
  {proposalno: '', proposalholder:'', agent: '', branch: '', issueddate: '', action: ''},
  {proposalno: '', proposalholder:'', agent: '', branch: '', issueddate: '', action: ''},
];


@Component({
  selector: 'app-medical-history',
  templateUrl: './medical-history.component.html',
  styleUrls: ['./medical-history.component.css']
})
export class MedicalHistoryComponent implements OnInit {

  displayedColumns: string[] = ['proposalno', 'proposalholder', 'agent', 'branch', 'issueddate', 'action'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
