import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoMedicalHistoryComponent } from './auto-medical-history.component';

describe('AutoMedicalHistoryComponent', () => {
  let component: AutoMedicalHistoryComponent;
  let fixture: ComponentFixture<AutoMedicalHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoMedicalHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoMedicalHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
