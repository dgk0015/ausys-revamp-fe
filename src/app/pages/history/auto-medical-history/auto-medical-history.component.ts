import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  proposalno: string;
  proposalholder: string;
  agent: string;
  branch: string;
  proposaldate: string;
  action: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {proposalno: '', proposalholder:'', agent: '', branch: '', proposaldate: '', action: ''},
  {proposalno: '', proposalholder:'', agent: '', branch: '', proposaldate: '', action: ''},
  {proposalno: '', proposalholder:'', agent: '', branch: '', proposaldate: '', action: ''},
];

@Component({
  selector: 'app-auto-medical-history',
  templateUrl: './auto-medical-history.component.html',
  styleUrls: ['./auto-medical-history.component.css']
})
export class AutoMedicalHistoryComponent implements OnInit {

  displayedColumns: string[] = ['proposalno', 'proposalholder', 'agent', 'branch', 'proposaldate', 'action'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
