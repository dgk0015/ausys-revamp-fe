import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualMedicalHistoryComponent } from './manual-medical-history.component';

describe('ManualMedicalHistoryComponent', () => {
  let component: ManualMedicalHistoryComponent;
  let fixture: ComponentFixture<ManualMedicalHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualMedicalHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualMedicalHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
