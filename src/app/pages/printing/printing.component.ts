import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  proposalno: string;
  branch: string;
  agent: string;
  proposalholder: string;
  mainur: string;
  acceptedtime: string;
  anbp: string;
  freq: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {proposalno: '', branch: '', agent: '', proposalholder: '', mainur: '', acceptedtime: '', anbp: '', freq: ''},
  {proposalno: '', branch: '', agent: '', proposalholder: '', mainur: '', acceptedtime: '', anbp: '', freq: ''},
  {proposalno: '', branch: '', agent: '', proposalholder: '', mainur: '', acceptedtime: '', anbp: '', freq: ''},
];

@Component({
  selector: 'app-printing',
  templateUrl: './printing.component.html',
  styleUrls: ['./printing.component.css']
})
export class PrintingComponent implements OnInit {

  displayedColumns: string[] = ['proposalno', 'branch', 'agent', 'proposalholder', 'mainur', 'acceptedtime', 'anbp', 'freq'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
