import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stp',
  templateUrl: './stp.component.html',
  styleUrls: ['./stp.component.css']
})
export class StpComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }


  public barChartOptionsProduction = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public barChartLabelsProduction = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public barChartTypeProduction = 'line';
  public barChartLegendProduction = true;
  public barChartDataProduction = [
    {
      data: [75, 87.5, 86.7, 86.58, 85.64, 85.79, 87.76, 87.85, 86.05, 88.41, 87.52, 88.69], 
      label: 'STP',
      pointBackgroundColor: "#33FF71 ", 
      borderColor: "#33FF71 ", 
      fill: false
    },
    { 
      data: [25, 12.220001, 12.220012, 13.419998, 13.411000, 14.209999, 14.209888, 12.150002, 12.150010, 11.589996, 11.589999, 11.309998], 
      label: 'Non STP',
      pointBackgroundColor: "#5EA4CF ", 
      borderColor: "#5EA4CF ", 
      fill: false
    }
  ];



  public PieChartOptionsProduction = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public PieChartLabelsProduction = ['STP', 'Non STP'];
  public PieChartTypeProduction = 'doughnut';
  public PieChartLegendProduction = true;
  public PieChartDataProduction = [
    {
      data: [44.83, 55.17], 
      pointBackgroundColor: "#33FF71 ", 
      backgroundColor: ["#33FF71", "#CBCBCB"],
      borderColor: ["#33FF71","#CBCBCB"],
      fill: false
    },
  ];

}
