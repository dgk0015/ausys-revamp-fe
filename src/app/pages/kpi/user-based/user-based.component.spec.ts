import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBasedComponent } from './user-based.component';

describe('UserBasedComponent', () => {
  let component: UserBasedComponent;
  let fixture: ComponentFixture<UserBasedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBasedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBasedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
