import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-based',
  templateUrl: './user-based.component.html',
  styleUrls: ['./user-based.component.css']
})
export class UserBasedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public barChartOptionsProduction = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public barChartLabelsProduction = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public barChartTypeProduction = 'line';
  public barChartLegendProduction = true;
  public barChartDataProduction = [
    {
      data: [75, 87.5, 86.7, 86.58, 85.64, 85.79, 87.76, 87.85, 86.05, 88.41, 87.52, 88.69], 
      label: 'alils014',
      pointBackgroundColor: "#060301", 
      borderColor: "#060301", 
      fill: false
    },
    { 
      data: [25, 12.220001, 12.220012, 13.419998, 13.411000, 14.209999, 14.209888, 12.150002, 12.150010, 11.589996, 11.589999, 11.309998], 
      label: 'ImranaA',
      pointBackgroundColor: "#870EE7", 
      borderColor: "#870EE7", 
      fill: false
    },
    { 
      data: [25, 12.220001, 12.220012, 13.419998, 13.411000, 14.209999, 14.209888, 12.150002, 12.150010, 11.589996, 11.589999, 11.309998], 
      label: 'DilanT',
      pointBackgroundColor: "#7EF80C", 
      borderColor: "#7EF80C", 
      fill: false
    },
    { 
      data: [25, 12.220001, 12.220012, 13.419998, 13.411000, 14.209999, 14.209888, 12.150002, 12.150010, 11.589996, 11.589999, 11.309998], 
      label: 'vilasinip',
      pointBackgroundColor: "#117CD5", 
      borderColor: "#117CD5", 
      fill: false
    },
    { 
      data: [25, 12.220001, 12.220012, 13.419998, 13.411000, 14.209999, 14.209888, 12.150002, 12.150010, 11.589996, 11.589999, 11.309998], 
      label: 'imrana',
      pointBackgroundColor: "#066081", 
      borderColor: "#066081", 
      fill: false
    },
    { 
      data: [25, 12.220001, 12.220012, 13.419998, 13.411000, 14.209999, 14.209888, 12.150002, 12.150010, 11.589996, 11.589999, 11.309998], 
      label: 'gayanih',
      pointBackgroundColor: "#31FF03", 
      borderColor: "#31FF03", 
      fill: false
    },
    { 
      data: [25, 12.220001, 12.220012, 13.419998, 13.411000, 14.209999, 14.209888, 12.150002, 12.150010, 11.589996, 11.589999, 11.309998], 
      label: 'MunisamyJ',
      pointBackgroundColor: "#427936 ", 
      borderColor: "#427936 ", 
      fill: false
    },
    { 
      data: [25, 12.220001, 12.220012, 13.419998, 13.411000, 14.209999, 14.209888, 12.150002, 12.150010, 11.589996, 11.589999, 11.309998], 
      label: 'umayangaf',
      pointBackgroundColor: "#05C9F9 ", 
      borderColor: "#05C9F9 ", 
      fill: false
    },
    { 
      data: [25, 12.220001, 12.220012, 13.419998, 13.411000, 14.209999, 14.209888, 12.150002, 12.150010, 11.589996, 11.589999, 11.309998], 
      label: 'myarena/ausys',
      pointBackgroundColor: "#A379F9 ", 
      borderColor: "#A379F9 ", 
      fill: false
    },
    { 
      data: [25, 12.220001, 12.220012, 13.419998, 13.411000, 14.209999, 14.209888, 12.150002, 12.150010, 11.589996, 11.589999, 11.309998], 
      label: 'rucherap',
      pointBackgroundColor: "#483966 ", 
      borderColor: "#483966 ", 
      fill: false
    },
    { 
      data: [25, 12.220001, 12.220012, 13.419998, 13.411000, 14.209999, 14.209888, 12.150002, 12.150010, 11.589996, 11.589999, 11.309998], 
      label: 'myarena/ausys_IMG',
      pointBackgroundColor: "#DFB4EB ", 
      borderColor: "#DFB4EB ", 
      fill: false
    },
    { 
      data: [25, 12.220001, 12.220012, 13.419998, 13.411000, 14.209999, 14.209888, 12.150002, 12.150010, 11.589996, 11.589999, 11.309998], 
      label: 'malithb',
      pointBackgroundColor: "#7405A5 ", 
      borderColor: "#7405A5 ", 
      fill: false
    }
  ];



  public PieChartOptionsProduction = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public PieChartLabelsProduction = ['umayangaf', 'myarena/ausys_IMG', 'myarena/ausys', 'malithb', 'imrana', 'gayanih'];
  public PieChartTypeProduction = 'doughnut';
  public PieChartLegendProduction = true;
  public PieChartDataProduction = [
    {
      data: [36.1, 18.1, 0.8, 3.3, 0.8, 12.0], 
      pointBackgroundColor: "#33FF71 ", 
      backgroundColor: ["#33FF71", "#CBCBCB", "#5EA4CF", "#ED43D6", "#060301", "#E15D1B"],
      borderColor: ["#33FF71", "#CBCBCB", "#5EA4CF", "#ED43D6", "#060301", "#E15D1B"],
      fill: false
    },
  ];

}
