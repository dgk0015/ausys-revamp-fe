import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.css']
})
export class PolicyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public barChartOptionsProduction = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public barChartLabelsProduction = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public barChartTypeProduction = 'line';
  public barChartLegendProduction = true;
  public barChartDataProduction = [
    {
      data: [248, 250, 255, 240, 240, 249, 240, 250, 230, 230, 100, 10], 
      label: 'STP',
      pointBackgroundColor: "#ECE318", 
      borderColor: "#ECE318", 
      fill: false
    },
    { 
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
      label: 'Non STP',
      pointBackgroundColor: "#E81149", 
      borderColor: "#E81149", 
      fill: false
    },
    { 
      data: [1700, 1300, 1745, 1010, 1020, 745, 550, 600, 510, 740, 550, 540], 
      label: 'Pending',
      pointBackgroundColor: "#66E811", 
      borderColor: "#66E811", 
      fill: false
    },
    { 
      data: [20, 20, 20, 5, 20, 10, 5, 3, 3, 3, 3, 3], 
      label: 'Total',
      pointBackgroundColor: "#5EA4CF", 
      borderColor: "#5EA4CF ", 
      fill: false
    }
  ];


}
