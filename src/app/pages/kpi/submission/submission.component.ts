import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-submission',
  templateUrl: './submission.component.html',
  styleUrls: ['./submission.component.css']
})
export class SubmissionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public barChartOptionsProduction = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public barChartLabelsProduction = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public barChartTypeProduction = 'bar';
  public barChartLegendProduction = true;
  public barChartDataProduction = [
    {
      data: [80, 56, 57, 95.86, 97.32, 97.77, 97.26, 95.55, 97.25, 97.18, 98.24, 99.56], 
      label: 'MOBILE',
      pointBackgroundColor: "#33FF71 ", 
      borderColor: "#33FF71 ", 
      fill: false
    },
    { 
      data: [20, 44, 43, 4.14, 2.6, 2.23, 2.74, 4.45, 2.75, 2.82, 1.76, 0.44], 
      label: 'MANUAL',
      pointBackgroundColor: "#5EA4CF ", 
      borderColor: "#5EA4CF ", 
      fill: false
    }
  ];


  public bar2ChartOptionsProduction = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public bar2ChartLabelsProduction = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public bar2ChartTypeProduction = 'bar';
  public bar2ChartLegendProduction = true;
  public bar2ChartDataProduction = [
    {
      data: [80, 56, 57, 95.86, 97.32, 97.77, 97.26, 95.55, 97.25, 97.18, 98.24, 99.56], 
      label: 'MOBILE',
      pointBackgroundColor: "#33FF71 ", 
      borderColor: "#33FF71 ", 
      fill: false
    },
    { 
      data: [20, 44, 43, 4.14, 2.6, 2.23, 2.74, 4.45, 2.75, 2.82, 1.76, 0.44], 
      label: 'MANUAL',
      pointBackgroundColor: "#5EA4CF ", 
      borderColor: "#5EA4CF ", 
      fill: false
    }
  ];

}
