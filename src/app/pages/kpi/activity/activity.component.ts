import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

  public barChartOptionsProduction = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public barChartLabelsProduction = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public barChartTypeProduction = 'line';
  public barChartLegendProduction = true;
  public barChartDataProduction = [
    {
      data: [248, 250, 255, 240, 240, 249, 240, 250, 230, 230, 100, 10], 
      label: 'Ausys Accepted',
      pointBackgroundColor: "#ECE318", 
      borderColor: "#ECE318", 
      fill: false
    },
    { 
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
      label: 'Ausys Pending',
      pointBackgroundColor: "#E81149", 
      borderColor: "#E81149", 
      fill: false
    },
    { 
      data: [1700, 1300, 1745, 1010, 1020, 745, 550, 600, 510, 740, 550, 540], 
      label: 'UW Accepted',
      pointBackgroundColor: "#66E811", 
      borderColor: "#66E811", 
      fill: false
    },
    { 
      data: [20, 20, 20, 5, 20, 10, 5, 3, 3, 3, 3, 3], 
      label: 'UW Pending',
      pointBackgroundColor: "#5EA4CF", 
      borderColor: "#5EA4CF ", 
      fill: false
    },
    { 
      data: [100, 100, 150, 100, 150, 240, 230, 350, 310, 230, 130, 11], 
      label: 'Unattended',
      pointBackgroundColor: "#ED43D6", 
      borderColor: "#ED43D6", 
      fill: false
    }
  ];



  public PieChartOptionsProduction = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public PieChartLabelsProduction = ['Ausys Accepted', 'Ausys Pending', 'UW Accepted', 'UW Pending', 'Unattended'];
  public PieChartTypeProduction = 'doughnut';
  public PieChartLegendProduction = true;
  public PieChartDataProduction = [
    {
      data: [0, 3.37, 33.15, 17.98, 45.51], 
      pointBackgroundColor: "#33FF71", 
      backgroundColor: ["#ECE318", "#E81149", "#66E811", "#5EA4CF", "#ED43D6"],
      borderColor: ["#ECE318", "#E81149", "#66E811", "#5EA4CF", "#ED43D6"],
      fill: false
    },
  ];

}
