import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adoption',
  templateUrl: './adoption.component.html',
  styleUrls: ['./adoption.component.css']
})
export class AdoptionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public barChartOptionsProduction = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public barChartLabelsProduction = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public barChartTypeProduction = 'line';
  public barChartLegendProduction = true;
  public barChartDataProduction = [
    {
      data: [80, 56, 57, 95.86, 97.32, 97.77, 97.26, 95.55, 97.25, 97.18, 98.24, 99.56], 
      label: 'MOBILE',
      pointBackgroundColor: "#33FF71 ", 
      borderColor: "#33FF71 ", 
      fill: false
    },
    { 
      data: [20, 44, 43, 4.14, 2.6, 2.23, 2.74, 4.45, 2.75, 2.82, 1.76, 0.44], 
      label: 'MANUAL',
      pointBackgroundColor: "#5EA4CF ", 
      borderColor: "#5EA4CF ", 
      fill: false
    }
  ];



  public PieChartOptionsProduction = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public PieChartLabelsProduction = ['MOBILE', 'MANUAL'];
  public PieChartTypeProduction = 'doughnut';
  public PieChartLegendProduction = true;
  public PieChartDataProduction = [
    {
      data: [44.83, 55.17], 
      pointBackgroundColor: "#33FF71 ", 
      backgroundColor: ["#33FF71", "#CBCBCB"],
      borderColor: ["#33FF71","#CBCBCB"],
      fill: false
    },
  ];

}
