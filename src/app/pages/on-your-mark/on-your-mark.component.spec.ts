import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnYourMarkComponent } from './on-your-mark.component';

describe('OnYourMarkComponent', () => {
  let component: OnYourMarkComponent;
  let fixture: ComponentFixture<OnYourMarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnYourMarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnYourMarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
