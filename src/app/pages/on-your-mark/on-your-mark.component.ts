import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  proposalno: string;
  branch: string;
  agent: string;
  mainur: string;
  type: string;
  status: string;
  update: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {proposalno: '', branch: '', agent: '',  mainur: '', type: '', status: '', update: ''},
  {proposalno: '', branch: '', agent: '',  mainur: '', type: '', status: '', update: ''},
  {proposalno: '', branch: '', agent: '',  mainur: '', type: '', status: '', update: ''},
];

@Component({
  selector: 'app-on-your-mark',
  templateUrl: './on-your-mark.component.html',
  styleUrls: ['./on-your-mark.component.css']
})
export class OnYourMarkComponent implements OnInit {

  displayedColumns: string[] = ['proposalno', 'branch', 'agent', 'mainur', 'type', 'status', 'update'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
