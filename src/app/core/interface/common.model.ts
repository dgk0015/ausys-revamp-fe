export interface Month {
    value: string;
    month: string;
}

export interface Year {
    value: string;
    year: string;
}

export interface MainCategory {
    value: string;
    category: string;
}

export interface Eligibility {
    value: string;
    eligibility: string;
}

export interface Branch {
    value: string;
    branch: string;
}