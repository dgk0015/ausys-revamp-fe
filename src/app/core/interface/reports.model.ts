export interface CutOffDateTimeTableData {
    proposalNo: string;
    branch: string;
    agent: string;
    mainUrType: string;
    status: string;
}

export interface Status {
    value: string;
    status: string;
}